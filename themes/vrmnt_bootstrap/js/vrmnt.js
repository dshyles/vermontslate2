(function ($) {
  Drupal.behaviors.merced_misc = {
    attach: function (context, settings) {
      $(window).load(function() {

				$("a").click(function() {
					var toId = $(this).attr('href').split('#')[1];
					if (toId.length) {
						if(toId == "page") {
						//fix for the fact that #page is not a present ID and I don't feel like changing every menu
				    $('html,body').animate({
  	  		    scrollTop: $("#main-content").offset().top},
	      		  'slow');
						} else {
				    $('html,body').animate({
  	  		    scrollTop: $("#"+toId).offset().top},
	      		  'slow');
						}
					}
				});

      });//END WINDOW LOAD    
    }
  }

})(jQuery);

