<?php
/**
 * @file
 * project_content_type.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function project_content_type_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_applications|node|content_project|default';
  $field_group->group_name = 'group_applications';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'applications',
    'weight' => '-1',
    'children' => array(
      0 => 'field_applications_0',
      1 => 'field_show_on_application_page',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_applications|node|content_project|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_applications|node|content_project|form';
  $field_group->group_name = 'group_applications';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'applications',
    'weight' => '6',
    'children' => array(
      0 => 'field_applications_0',
      1 => 'field_show_on_application_page',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_applications|node|content_project|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_applications|node|content_project|teaser';
  $field_group->group_name = 'group_applications';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'applications',
    'weight' => '-1',
    'children' => array(
      0 => 'field_applications_0',
      1 => 'field_show_on_application_page',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_applications|node|content_project|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_architects|node|content_project|default';
  $field_group->group_name = 'group_architects';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'architects',
    'weight' => '-2',
    'children' => array(
      0 => 'field_architect',
      1 => 'field_architect_display_name',
      2 => 'field_designer',
      3 => 'field_design_architect',
      4 => 'field_executive_architect',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_architects|node|content_project|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_architects|node|content_project|form';
  $field_group->group_name = 'group_architects';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'architects',
    'weight' => '4',
    'children' => array(
      0 => 'field_design_architect',
      1 => 'field_executive_architect',
      2 => 'field_architect',
      3 => 'field_designer',
      4 => 'field_architect_display_name',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_architects|node|content_project|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_architects|node|content_project|teaser';
  $field_group->group_name = 'group_architects';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'architects',
    'weight' => '-2',
    'children' => array(
      0 => 'field_architect',
      1 => 'field_architect_display_name',
      2 => 'field_designer',
      3 => 'field_design_architect',
      4 => 'field_executive_architect',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_architects|node|content_project|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_awards__description|node|content_project|default';
  $field_group->group_name = 'group_awards__description';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'awards',
    'weight' => '-2',
    'children' => array(
      0 => 'field_awards',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_awards__description|node|content_project|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_awards__description|node|content_project|form';
  $field_group->group_name = 'group_awards__description';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'awards',
    'weight' => '5',
    'children' => array(
      0 => 'field_awards',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_awards__description|node|content_project|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_awards__description|node|content_project|teaser';
  $field_group->group_name = 'group_awards__description';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'awards',
    'weight' => '-2',
    'children' => array(
      0 => 'field_awards',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_awards__description|node|content_project|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_photo_credits|node|content_project|default';
  $field_group->group_name = 'group_photo_credits';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'photos',
    'weight' => '-3',
    'children' => array(
      0 => 'field_photos',
      1 => 'field_portfolio_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_photo_credits|node|content_project|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_photo_credits|node|content_project|form';
  $field_group->group_name = 'group_photo_credits';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'photos',
    'weight' => '3',
    'children' => array(
      0 => 'field_photos',
      1 => 'field_portfolio_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_photo_credits|node|content_project|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_photo_credits|node|content_project|teaser';
  $field_group->group_name = 'group_photo_credits';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'photos',
    'weight' => '-3',
    'children' => array(
      0 => 'field_photos',
      1 => 'field_portfolio_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_photo_credits|node|content_project|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_stones|node|content_project|default';
  $field_group->group_name = 'group_stones';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'stones',
    'weight' => '-1',
    'children' => array(
      0 => 'field_other_misc_information',
      1 => 'field_stones',
      2 => 'field_material',
      3 => 'field_stones_noshow',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_stones|node|content_project|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_stones|node|content_project|form';
  $field_group->group_name = 'group_stones';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'stones',
    'weight' => '7',
    'children' => array(
      0 => 'field_material',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_stones|node|content_project|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_stones|node|content_project|teaser';
  $field_group->group_name = 'group_stones';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_project';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'stones',
    'weight' => '-1',
    'children' => array(
      0 => 'field_other_misc_information',
      1 => 'field_stones',
      2 => 'field_material',
      3 => 'field_stones_noshow',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_stones|node|content_project|teaser'] = $field_group;

  return $export;
}
