<?php
/**
 * @file
 * project_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function project_content_type_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function project_content_type_flag_default_flags() {
  $flags = array();
  // Exported flag: "Portfolio Page".
  $flags['portfolio'] = array(
    'content_type' => 'node',
    'title' => 'Portfolio Page',
    'global' => '1',
    'types' => array(
      0 => 'content_project',
    ),
    'flag_short' => 'Add to Portfolio',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove from Portfolio',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '4',
        1 => '3',
      ),
      'unflag' => array(
        0 => '4',
        1 => '3',
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 0,
    'show_on_form' => 1,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'project_content_type',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}

/**
 * Implements hook_image_default_styles().
 */
function project_content_type_image_default_styles() {
  $styles = array();

  // Exported image style: thumbs_apps_projects
  $styles['thumbs_apps_projects'] = array(
    'name' => 'thumbs_apps_projects',
    'effects' => array(
      4 => array(
        'label' => 'Resize',
        'help' => 'Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => '100',
          'height' => '100',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: thumbs_portfolio
  $styles['thumbs_portfolio'] = array(
    'name' => 'thumbs_portfolio',
    'effects' => array(
      5 => array(
        'label' => 'Resize',
        'help' => 'Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => '150',
          'height' => '150',
        ),
        'weight' => '1',
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function project_content_type_node_info() {
  $items = array(
    'content_project' => array(
      'name' => t('Project'),
      'base' => 'node_content',
      'description' => t('Create a project.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Create a project.'),
    ),
  );
  return $items;
}
