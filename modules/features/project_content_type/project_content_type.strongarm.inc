<?php
/**
 * @file
 * project_content_type.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function project_content_type_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'additional_settings__active_tab_content_project';
  $strongarm->value = 'edit-submission';
  $export['additional_settings__active_tab_content_project'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'excerpt_options_content_project';
  $strongarm->value = '0';
  $export['excerpt_options_content_project'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_content_project';
  $strongarm->value = array(
    0 => 'navigation',
  );
  $export['menu_options_content_project'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_content_project';
  $strongarm->value = 'navigation:0';
  $export['menu_parent_content_project'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'minimum_content_project_size';
  $strongarm->value = '0';
  $export['minimum_content_project_size'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_content_project';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_content_project'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_content_project';
  $strongarm->value = '0';
  $export['node_preview_content_project'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_content_project';
  $strongarm->value = 0;
  $export['node_submitted_content_project'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_content_project_pattern';
  $strongarm->value = 'project/[node:title]';
  $export['pathauto_node_content_project_pattern'] = $strongarm;

  return $export;
}
