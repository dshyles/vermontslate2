<?php
/**
 * @file
 * news_content_type.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function news_content_type_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_news_body__teaser_text|node|content_news|default';
  $field_group->group_name = 'group_news_body__teaser_text';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_news';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'news body / teaser text',
    'weight' => '-3',
    'children' => array(
      0 => 'field_news_body',
      1 => 'field_news_teaser_text',
    ),
    'format_type' => 'default',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_news_body__teaser_text|node|content_news|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_news_body__teaser_text|node|content_news|form';
  $field_group->group_name = 'group_news_body__teaser_text';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'news body / teaser text',
    'weight' => '-3',
    'children' => array(
      0 => 'field_news_body',
      1 => 'field_news_teaser_text',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsed',
      'instance_settings' => array(),
    ),
  );
  $export['group_news_body__teaser_text|node|content_news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_news_body__teaser_text|node|content_news|teaser';
  $field_group->group_name = 'group_news_body__teaser_text';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_news';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'news body / teaser text',
    'weight' => '-3',
    'children' => array(
      0 => 'field_news_body',
      1 => 'field_news_teaser_text',
    ),
    'format_type' => 'default',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_news_body__teaser_text|node|content_news|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_news_photos|node|content_news|default';
  $field_group->group_name = 'group_news_photos';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_news';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'photos',
    'weight' => '-2',
    'children' => array(
      0 => 'field_news_photo_credits',
      1 => 'field_news_pictures',
    ),
    'format_type' => 'default',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_news_photos|node|content_news|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_news_photos|node|content_news|form';
  $field_group->group_name = 'group_news_photos';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'photos',
    'weight' => '-2',
    'children' => array(
      0 => 'field_news_photo_credits',
      1 => 'field_news_pictures',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsed',
      'instance_settings' => array(),
    ),
  );
  $export['group_news_photos|node|content_news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_news_photos|node|content_news|teaser';
  $field_group->group_name = 'group_news_photos';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_news';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'photos',
    'weight' => '-2',
    'children' => array(
      0 => 'field_news_photo_credits',
      1 => 'field_news_pictures',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_news_photos|node|content_news|teaser'] = $field_group;

  return $export;
}
