<?php
/**
 * @file
 * surplus_content_type.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function surplus_content_type_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'additional_settings__active_tab_surplus';
  $strongarm->value = 'edit-submission';
  $export['additional_settings__active_tab_surplus'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_surplus';
  $strongarm->value = array(
    0 => 'navigation',
  );
  $export['menu_options_surplus'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_surplus';
  $strongarm->value = 'navigation:0';
  $export['menu_parent_surplus'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_surplus';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_surplus'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_surplus';
  $strongarm->value = '0';
  $export['node_preview_surplus'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_surplus';
  $strongarm->value = 0;
  $export['node_submitted_surplus'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_surplus_pattern';
  $strongarm->value = 'surplus/[node:title]';
  $export['pathauto_node_surplus_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'teaser_length_surplus';
  $strongarm->value = '600';
  $export['teaser_length_surplus'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_surplus';
  $strongarm->value = '0';
  $export['upload_surplus'] = $strongarm;

  return $export;
}
