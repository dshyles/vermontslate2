<?php
/**
 * @file
 * surplus_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function surplus_content_type_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function surplus_content_type_flag_default_flags() {
  $flags = array();
  // Exported flag: "Surplus First".
  $flags['surplus_first'] = array(
    'content_type' => 'node',
    'title' => 'Surplus First',
    'global' => '1',
    'types' => array(
      0 => 'surplus',
    ),
    'flag_short' => 'Add to Surplus First',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove from Surplus First',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '4',
        1 => '3',
      ),
      'unflag' => array(
        0 => '4',
        1 => '3',
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 0,
    'show_on_form' => 1,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'surplus_content_type',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Surplus Second".
  $flags['surplus_second'] = array(
    'content_type' => 'node',
    'title' => 'Surplus Second',
    'global' => '1',
    'types' => array(
      0 => 'surplus',
    ),
    'flag_short' => 'Add to Surplus Second',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove from Surplus Second',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '4',
        1 => '3',
      ),
      'unflag' => array(
        0 => '4',
        1 => '3',
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 0,
    'show_on_form' => 1,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'surplus_content_type',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}

/**
 * Implements hook_node_info().
 */
function surplus_content_type_node_info() {
  $items = array(
    'surplus' => array(
      'name' => t('Surplus'),
      'base' => 'node_content',
      'description' => t('Add surplus items for any given stone.'),
      'has_title' => '1',
      'title_label' => t('Surplus Title'),
      'help' => t('Here you can define a surplus item for any stone in the system. The stone does not need to appear on the stones page. Give the surplus a title to help you find and delete it later.'),
    ),
  );
  return $items;
}
