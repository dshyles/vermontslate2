<?php
/**
 * @file
 * stone_content_type.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function stone_content_type_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'additional_settings__active_tab_content_stone';
  $strongarm->value = 'edit-menu';
  $export['additional_settings__active_tab_content_stone'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'excerpt_options_content_stone';
  $strongarm->value = '0';
  $export['excerpt_options_content_stone'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'image_attach_content_stone';
  $strongarm->value = '1';
  $export['image_attach_content_stone'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_content_stone';
  $strongarm->value = array(
    0 => 'navigation',
  );
  $export['menu_options_content_stone'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_content_stone';
  $strongarm->value = 'navigation:0';
  $export['menu_parent_content_stone'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'minimum_content_stone_size';
  $strongarm->value = '0';
  $export['minimum_content_stone_size'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_content_stone';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_content_stone'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_content_stone';
  $strongarm->value = '0';
  $export['node_preview_content_stone'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_content_stone';
  $strongarm->value = 0;
  $export['node_submitted_content_stone'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_content_stone_pattern';
  $strongarm->value = 'stones/[node:taxonomy_vocabulary_2]/[node:title]';
  $export['pathauto_node_content_stone_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'teaser_length_content_stone';
  $strongarm->value = '600';
  $export['teaser_length_content_stone'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_content_stone';
  $strongarm->value = '0';
  $export['upload_content_stone'] = $strongarm;

  return $export;
}
