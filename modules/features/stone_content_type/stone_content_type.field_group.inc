<?php
/**
 * @file
 * stone_content_type.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function stone_content_type_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_description|node|content_stone|default';
  $field_group->group_name = 'group_description';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_stone';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'description',
    'weight' => '0',
    'children' => array(
      0 => 'field_description',
      1 => 'field_stone_description',
    ),
    'format_type' => 'no_style',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_description|node|content_stone|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_description|node|content_stone|form';
  $field_group->group_name = 'group_description';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_stone';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'description',
    'weight' => '3',
    'children' => array(
      0 => 'field_stone_description',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_description|node|content_stone|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_description|node|content_stone|teaser';
  $field_group->group_name = 'group_description';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_stone';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'description',
    'weight' => '0',
    'children' => array(
      0 => 'field_description',
      1 => 'field_stone_description',
    ),
    'format_type' => 'no_style',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_description|node|content_stone|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_finishes_0|node|content_stone|default';
  $field_group->group_name = 'group_finishes_0';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_stone';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'finishes',
    'weight' => '-1',
    'children' => array(
      0 => 'field_finishes',
      1 => 'field_finishes_description',
    ),
    'format_type' => 'no_style',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_finishes_0|node|content_stone|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_finishes_0|node|content_stone|form';
  $field_group->group_name = 'group_finishes_0';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_stone';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'finishes',
    'weight' => '5',
    'children' => array(
      0 => 'field_finishes_description',
      1 => 'field_finishes',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_finishes_0|node|content_stone|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_finishes_0|node|content_stone|teaser';
  $field_group->group_name = 'group_finishes_0';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_stone';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'finishes',
    'weight' => '-1',
    'children' => array(
      0 => 'field_finishes',
      1 => 'field_finishes_description',
    ),
    'format_type' => 'no_style',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_finishes_0|node|content_stone|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_specs|node|content_stone|default';
  $field_group->group_name = 'group_specs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_stone';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'specs',
    'weight' => '-1',
    'children' => array(
      0 => 'field_cost_estimates',
      1 => 'field_drafting',
      2 => 'field_edge_details',
      3 => 'field_exposure',
      4 => 'field_finishes_0',
      5 => 'field_headlap',
      6 => 'field_installation',
      7 => 'field_joint_widths',
      8 => 'field_lettering',
      9 => 'field_maintenance_0',
      10 => 'field_nail_holes',
      11 => 'field_patterns',
      12 => 'field_performance',
      13 => 'field_products',
      14 => 'field_references',
      15 => 'field_sizes',
      16 => 'field_technical_support',
      17 => 'field_thickness',
      18 => 'field_weathering_characterist_0',
      19 => 'field_astm_results',
      20 => 'field_color',
      21 => 'field_maintenance',
      22 => 'field_properties',
      23 => 'field_samples',
      24 => 'field_suitability',
      25 => 'field_surplus_only',
      26 => 'field_weathering_characteristic',
    ),
    'format_type' => 'no_style',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_specs|node|content_stone|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_specs|node|content_stone|form';
  $field_group->group_name = 'group_specs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_stone';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'specs',
    'weight' => '4',
    'children' => array(
      0 => 'field_suitability',
      1 => 'field_color',
      2 => 'field_weathering_characteristic',
      3 => 'field_properties',
      4 => 'field_maintenance',
      5 => 'field_samples',
      6 => 'field_astm_results',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_specs|node|content_stone|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_specs|node|content_stone|teaser';
  $field_group->group_name = 'group_specs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_stone';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'specs',
    'weight' => '-1',
    'children' => array(
      0 => 'field_cost_estimates',
      1 => 'field_drafting',
      2 => 'field_edge_details',
      3 => 'field_exposure',
      4 => 'field_finishes_0',
      5 => 'field_headlap',
      6 => 'field_installation',
      7 => 'field_joint_widths',
      8 => 'field_lettering',
      9 => 'field_maintenance_0',
      10 => 'field_nail_holes',
      11 => 'field_patterns',
      12 => 'field_performance',
      13 => 'field_products',
      14 => 'field_references',
      15 => 'field_sizes',
      16 => 'field_technical_support',
      17 => 'field_thickness',
      18 => 'field_weathering_characterist_0',
      19 => 'field_astm_results',
      20 => 'field_color',
      21 => 'field_maintenance',
      22 => 'field_properties',
      23 => 'field_samples',
      24 => 'field_suitability',
      25 => 'field_surplus_only',
      26 => 'field_weathering_characteristic',
    ),
    'format_type' => 'no_style',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_specs|node|content_stone|teaser'] = $field_group;

  return $export;
}
