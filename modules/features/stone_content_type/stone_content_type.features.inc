<?php
/**
 * @file
 * stone_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function stone_content_type_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function stone_content_type_flag_default_flags() {
  $flags = array();
  // Exported flag: "Roofing Stones".
  $flags['roofing_stones'] = array(
    'content_type' => 'node',
    'title' => 'Roofing Stones',
    'global' => '1',
    'types' => array(
      0 => 'content_stone',
    ),
    'flag_short' => 'Add to Roofing Stones',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove from Roofing Stones',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '4',
        1 => '3',
      ),
      'unflag' => array(
        0 => '4',
        1 => '3',
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 0,
    'show_on_form' => 1,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'stone_content_type',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Stones Page".
  $flags['stones_page'] = array(
    'content_type' => 'node',
    'title' => 'Stones Page',
    'global' => '1',
    'types' => array(
      0 => 'content_stone',
    ),
    'flag_short' => 'Add to Stones Page',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove from Stones Page',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '4',
        1 => '3',
      ),
      'unflag' => array(
        0 => '4',
        1 => '3',
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 0,
    'show_on_form' => 1,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'stone_content_type',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Stones Project No-Show".
  $flags['stones_project_no_show'] = array(
    'content_type' => 'node',
    'title' => 'Stones Project No-Show',
    'global' => '1',
    'types' => array(
      0 => 'content_stone',
    ),
    'flag_short' => 'Add to Stones Project No-Show',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove from Stones Project No-Show',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '4',
        1 => '3',
      ),
      'unflag' => array(
        0 => '4',
        1 => '3',
      ),
    ),
    'show_on_page' => 0,
    'show_on_teaser' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'stone_content_type',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}

/**
 * Implements hook_image_default_styles().
 */
function stone_content_type_image_default_styles() {
  $styles = array();

  // Exported image style: thumbs_stone_roofing
  $styles['thumbs_stone_roofing'] = array(
    'name' => 'thumbs_stone_roofing',
    'effects' => array(
      6 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '100',
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: thumbs_stones
  $styles['thumbs_stones'] = array(
    'name' => 'thumbs_stones',
    'effects' => array(
      2 => array(
        'label' => 'Resize',
        'help' => 'Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => '100',
          'height' => '100',
        ),
        'weight' => '1',
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function stone_content_type_node_info() {
  $items = array(
    'content_stone' => array(
      'name' => t('Stone'),
      'base' => 'node_content',
      'description' => t('Create a stone.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Create a stone.'),
    ),
  );
  return $items;
}
