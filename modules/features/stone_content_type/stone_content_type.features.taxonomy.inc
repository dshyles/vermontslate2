<?php
/**
 * @file
 * stone_content_type.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function stone_content_type_taxonomy_default_vocabularies() {
  return array(
    'vocabulary_2' => array(
      'name' => 'Stones',
      'machine_name' => 'vocabulary_2',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
