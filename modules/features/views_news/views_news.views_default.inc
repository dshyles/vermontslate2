<?php
/**
 * @file
 * views_news.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function views_news_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'Spotlight';
  $view->description = 'Show news and spotlight views.';
  $view->tag = 'VSS';
  $view->base_table = 'node';
  $view->human_name = 'News / Spotlight';
  $view->core = 0;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['title'] = 'Spotlight';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'panels_fields';
  $handler->display->display_options['row_options']['hide_empty'] = 0;
  $handler->display->display_options['row_options']['layout'] = 'flexible:spotlight_panel';
  $handler->display->display_options['row_options']['regions'] = array(
    'title' => 'text',
    'field_news_pictures' => 'image',
    'field_stone_image' => 'image',
    'field_portfolio_image' => 'image',
    'field_news_teaser_text' => 'text',
    'field_description' => 'text',
    'field_stone_description' => 'text',
  );
  /* Header: Global: Text area */
  $handler->display->display_options['header']['text']['id'] = 'text';
  $handler->display->display_options['header']['text']['table'] = 'views';
  $handler->display->display_options['header']['text']['field'] = 'area';
  $handler->display->display_options['header']['text']['empty'] = FALSE;
  $handler->display->display_options['header']['text']['content'] = ' ';
  $handler->display->display_options['header']['text']['format'] = '1';
  $handler->display->display_options['header']['text']['tokenize'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_class'] = 'title';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: news photos */
  $handler->display->display_options['fields']['field_news_pictures']['id'] = 'field_news_pictures';
  $handler->display->display_options['fields']['field_news_pictures']['table'] = 'field_data_field_news_pictures';
  $handler->display->display_options['fields']['field_news_pictures']['field'] = 'field_news_pictures';
  $handler->display->display_options['fields']['field_news_pictures']['label'] = '';
  $handler->display->display_options['fields']['field_news_pictures']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_news_pictures']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_news_pictures']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_news_pictures']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_news_pictures']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_news_pictures']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_news_pictures']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_news_pictures']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_news_pictures']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_news_pictures']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_news_pictures']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_news_pictures']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_news_pictures']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_news_pictures']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_news_pictures']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_news_pictures']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_news_pictures']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_news_pictures']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_news_pictures']['settings'] = array(
    'image_style' => 'thumbs_teaser',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_news_pictures']['group_column'] = 'fid';
  $handler->display->display_options['fields']['field_news_pictures']['group_rows'] = 1;
  $handler->display->display_options['fields']['field_news_pictures']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_news_pictures']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_news_pictures']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['field_news_pictures']['field_api_classes'] = 0;
  /* Field: Content: stone image */
  $handler->display->display_options['fields']['field_stone_image']['id'] = 'field_stone_image';
  $handler->display->display_options['fields']['field_stone_image']['table'] = 'field_data_field_stone_image';
  $handler->display->display_options['fields']['field_stone_image']['field'] = 'field_stone_image';
  $handler->display->display_options['fields']['field_stone_image']['label'] = '';
  $handler->display->display_options['fields']['field_stone_image']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_stone_image']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_stone_image']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_stone_image']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_stone_image']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_stone_image']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_stone_image']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_stone_image']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_stone_image']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_stone_image']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_stone_image']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_stone_image']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_stone_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_stone_image']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_stone_image']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_stone_image']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_stone_image']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_stone_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_stone_image']['settings'] = array(
    'image_style' => 'thumbs_teaser',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_stone_image']['group_column'] = 'fid';
  $handler->display->display_options['fields']['field_stone_image']['field_api_classes'] = 0;
  /* Field: Content: project photos */
  $handler->display->display_options['fields']['field_portfolio_image']['id'] = 'field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['table'] = 'field_data_field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['field'] = 'field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['label'] = '';
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_portfolio_image']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_portfolio_image']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_portfolio_image']['settings'] = array(
    'image_style' => 'thumbs_teaser',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_portfolio_image']['group_column'] = 'fid';
  $handler->display->display_options['fields']['field_portfolio_image']['group_rows'] = 1;
  $handler->display->display_options['fields']['field_portfolio_image']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_portfolio_image']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_portfolio_image']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['field_api_classes'] = 0;
  /* Field: Content: news teaser text */
  $handler->display->display_options['fields']['field_news_teaser_text']['id'] = 'field_news_teaser_text';
  $handler->display->display_options['fields']['field_news_teaser_text']['table'] = 'field_data_field_news_teaser_text';
  $handler->display->display_options['fields']['field_news_teaser_text']['field'] = 'field_news_teaser_text';
  $handler->display->display_options['fields']['field_news_teaser_text']['label'] = '';
  $handler->display->display_options['fields']['field_news_teaser_text']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_news_teaser_text']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_news_teaser_text']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_news_teaser_text']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_news_teaser_text']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_news_teaser_text']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_news_teaser_text']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_news_teaser_text']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_news_teaser_text']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_news_teaser_text']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_news_teaser_text']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_news_teaser_text']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_news_teaser_text']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_news_teaser_text']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_news_teaser_text']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_news_teaser_text']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_news_teaser_text']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_news_teaser_text']['type'] = 'text_plain';
  $handler->display->display_options['fields']['field_news_teaser_text']['field_api_classes'] = 0;
  /* Field: Content: spotlight description */
  $handler->display->display_options['fields']['field_description']['id'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['table'] = 'field_data_field_description';
  $handler->display->display_options['fields']['field_description']['field'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['label'] = '';
  $handler->display->display_options['fields']['field_description']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_description']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_description']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_description']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_description']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_description']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_description']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_description']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_description']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_description']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_description']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_description']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_description']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_description']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_description']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_description']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_description']['type'] = 'text_plain';
  $handler->display->display_options['fields']['field_description']['settings'] = array(
    'trim_length' => '600',
  );
  $handler->display->display_options['fields']['field_description']['field_api_classes'] = 0;
  /* Field: Content: stone description */
  $handler->display->display_options['fields']['field_stone_description']['id'] = 'field_stone_description';
  $handler->display->display_options['fields']['field_stone_description']['table'] = 'field_data_field_stone_description';
  $handler->display->display_options['fields']['field_stone_description']['field'] = 'field_stone_description';
  $handler->display->display_options['fields']['field_stone_description']['label'] = '';
  $handler->display->display_options['fields']['field_stone_description']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_stone_description']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_stone_description']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_stone_description']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_stone_description']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_stone_description']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_stone_description']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_stone_description']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_stone_description']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_stone_description']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_stone_description']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_stone_description']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_stone_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_stone_description']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_stone_description']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_stone_description']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_stone_description']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_stone_description']['type'] = 'text_plain';
  $handler->display->display_options['fields']['field_stone_description']['field_api_classes'] = 0;
  /* Sort criterion: Content: Sticky */
  $handler->display->display_options['sorts']['sticky']['id'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['table'] = 'node';
  $handler->display->display_options['sorts']['sticky']['field'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['order'] = 'DESC';
  /* Sort criterion: Content: Updated date */
  $handler->display->display_options['sorts']['changed']['id'] = 'changed';
  $handler->display->display_options['sorts']['changed']['table'] = 'node';
  $handler->display->display_options['sorts']['changed']['field'] = 'changed';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'content_application' => 'content_application',
    'content_news' => 'content_news',
    'content_project' => 'content_project',
    'content_stone' => 'content_stone',
  );
  /* Filter criterion: Content: Promoted to front page */
  $handler->display->display_options['filters']['promote']['id'] = 'promote';
  $handler->display->display_options['filters']['promote']['table'] = 'node';
  $handler->display->display_options['filters']['promote']['field'] = 'promote';
  $handler->display->display_options['filters']['promote']['value'] = '1';

  /* Display: Spotlight */
  $handler = $view->new_display('page', 'Spotlight', 'Spotlight');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Spotlight';
  $handler->display->display_options['display_description'] = 'Show the front page spotlight view.';
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'content_application' => 'content_application',
    'content_news' => 'content_news',
    'content_project' => 'content_project',
    'content_stone' => 'content_stone',
  );
  /* Filter criterion: Content: Promoted to front page */
  $handler->display->display_options['filters']['promote']['id'] = 'promote';
  $handler->display->display_options['filters']['promote']['table'] = 'node';
  $handler->display->display_options['filters']['promote']['field'] = 'promote';
  $handler->display->display_options['filters']['promote']['value'] = '1';
  $handler->display->display_options['path'] = 'spotlight';

  /* Display: News */
  $handler = $view->new_display('page', 'News', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Recent News';
  $handler->display->display_options['display_description'] = 'Show the news page view.';
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'content_news' => 'content_news',
  );
  $handler->display->display_options['path'] = 'news';
  $export['Spotlight'] = $view;

  return $export;
}
