<?php
/**
 * @file
 * views_projects.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function views_projects_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'Portfolio';
  $view->description = 'portfolio view';
  $view->tag = 'VSS';
  $view->base_table = 'node';
  $view->human_name = 'Portfolio';
  $view->core = 0;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['title'] = 'portfolio';
  $handler->display->display_options['items_per_page'] = '0';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['fill_single_line'] = 1;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = 0;
  $handler->display->display_options['row_options']['default_field_elements'] = 1;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['text']['id'] = 'area';
  $handler->display->display_options['header']['text']['table'] = 'views';
  $handler->display->display_options['header']['text']['field'] = 'area';
  $handler->display->display_options['header']['text']['empty'] = FALSE;
  $handler->display->display_options['header']['text']['content'] = ' ';
  $handler->display->display_options['header']['text']['format'] = '1';
  /* Relationship: Flags: portfolio */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['label'] = 'flag portfolio';
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'portfolio';
  $handler->display->display_options['relationships']['flag_content_rel']['user_scope'] = 'any';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['external'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['nid']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['nid']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['html'] = 0;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nid']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['nid']['hide_empty'] = 0;
  $handler->display->display_options['fields']['nid']['empty_zero'] = 0;
  $handler->display->display_options['fields']['nid']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['nid']['link_to_node'] = 0;
  /* Field: Content: project photos */
  $handler->display->display_options['fields']['field_portfolio_image']['id'] = 'field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['table'] = 'field_data_field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['field'] = 'field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['label'] = '';
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_portfolio_image']['element_class'] = 'portfolio-thumb-image';
  $handler->display->display_options['fields']['field_portfolio_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_portfolio_image']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_portfolio_image']['settings'] = array(
    'image_style' => 'thumbs_portfolio',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_portfolio_image']['group_column'] = 'fid';
  $handler->display->display_options['fields']['field_portfolio_image']['group_rows'] = 1;
  $handler->display->display_options['fields']['field_portfolio_image']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_portfolio_image']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_portfolio_image']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['field_portfolio_image']['separator'] = '';
  $handler->display->display_options['fields']['field_portfolio_image']['field_api_classes'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['max_length'] = '25';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 1;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: architect display name */
  $handler->display->display_options['fields']['field_architect_display_name']['id'] = 'field_architect_display_name';
  $handler->display->display_options['fields']['field_architect_display_name']['table'] = 'field_data_field_architect_display_name';
  $handler->display->display_options['fields']['field_architect_display_name']['field'] = 'field_architect_display_name';
  $handler->display->display_options['fields']['field_architect_display_name']['label'] = '';
  $handler->display->display_options['fields']['field_architect_display_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_architect_display_name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_architect_display_name']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['field_architect_display_name']['alter']['path'] = 'node/[nid]';
  $handler->display->display_options['fields']['field_architect_display_name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_architect_display_name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_architect_display_name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_architect_display_name']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_architect_display_name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_architect_display_name']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_architect_display_name']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_architect_display_name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_architect_display_name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_architect_display_name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_architect_display_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_architect_display_name']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['field_architect_display_name']['hide_empty'] = 1;
  $handler->display->display_options['fields']['field_architect_display_name']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_architect_display_name']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_architect_display_name']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['field_architect_display_name']['settings'] = array(
    'trim_length' => '25',
  );
  $handler->display->display_options['fields']['field_architect_display_name']['field_api_classes'] = 0;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="portfolio-thumb-screen">
  <div class="portfolio-thumb-title">[title]</div>
  <div class="portfolio-architect">[field_architect_display_name]</div>
</div>';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
  $handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
  $handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 0;
  /* Sort criterion: Broken/missing handler */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'flag_content';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight']['relationship'] = 'flag_content_rel';
  /* Sort criterion: Content: Draggable views weight */
  $handler->display->display_options['sorts']['weight_1']['id'] = 'weight_1';
  $handler->display->display_options['sorts']['weight_1']['table'] = 'draggableviews_structure';
  $handler->display->display_options['sorts']['weight_1']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight_1']['draggableviews_setting_view'] = 'order_content:portfolio';
  $handler->display->display_options['sorts']['weight_1']['draggableviews_setting_new_items_bottom_list'] = 1;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 0;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    0 => 'content_project',
  );
  $handler->display->display_options['filters']['type']['group'] = 0;

  /* Display: Portfolio */
  $handler = $view->new_display('page', 'Portfolio', 'page_1');
  $handler->display->display_options['path'] = 'portfolio';
  $export['Portfolio'] = $view;

  return $export;
}
