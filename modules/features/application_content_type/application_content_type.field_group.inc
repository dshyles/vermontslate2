<?php
/**
 * @file
 * application_content_type.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function application_content_type_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_description__options|node|content_application|default';
  $field_group->group_name = 'group_description__options';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'description',
    'weight' => '-2',
    'children' => array(
      0 => 'field_description_0',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_description__options|node|content_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_description__options|node|content_application|form';
  $field_group->group_name = 'group_description__options';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'description',
    'weight' => '1',
    'children' => array(
      0 => 'field_description_0',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_description__options|node|content_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_description__options|node|content_application|teaser';
  $field_group->group_name = 'group_description__options';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'content_application';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'description',
    'weight' => '-2',
    'children' => array(
      0 => 'field_description_0',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $export['group_description__options|node|content_application|teaser'] = $field_group;

  return $export;
}
