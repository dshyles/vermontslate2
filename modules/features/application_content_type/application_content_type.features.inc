<?php
/**
 * @file
 * application_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function application_content_type_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function application_content_type_flag_default_flags() {
  $flags = array();
  // Exported flag: "Applications".
  $flags['applications'] = array(
    'content_type' => 'node',
    'title' => 'Applications',
    'global' => '1',
    'types' => array(
      0 => 'content_application',
    ),
    'flag_short' => 'Add to Applications',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove from Applications',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '4',
        1 => '3',
      ),
      'unflag' => array(
        0 => '4',
        1 => '3',
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 0,
    'show_on_form' => 1,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'application_content_type',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}

/**
 * Implements hook_node_info().
 */
function application_content_type_node_info() {
  $items = array(
    'content_application' => array(
      'name' => t('Application'),
      'base' => 'node_content',
      'description' => t('Create an application.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Create an application.'),
    ),
  );
  return $items;
}
