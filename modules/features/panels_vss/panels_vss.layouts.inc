<?php
/**
 * @file
 * panels_vss.layouts.inc
 */

/**
 * Implements hook_default_panels_layout().
 */
function panels_vss_default_panels_layout() {
  $export = array();

  $layout = new stdClass();
  $layout->disabled = FALSE; /* Edit this to true to make a default layout disabled initially */
  $layout->api_version = 1;
  $layout->name = 'spotlight_panel';
  $layout->admin_title = 'Spotlight Panel';
  $layout->admin_description = 'Provide a two column layout for the homepage spotlight section.';
  $layout->category = 'VSS';
  $layout->plugin = 'flexible';
  $layout->settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => 'spotlight',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => FALSE,
        'fixed_width' => '',
        'column_separation' => '1em',
        'region_separation' => '1em',
        'row_separation' => '1em',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'text',
          1 => 'image',
        ),
        'parent' => 'main',
      ),
      'text' => array(
        'type' => 'region',
        'title' => 'Text',
        'width' => '100',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      'image' => array(
        'type' => 'region',
        'title' => 'Image',
        'width' => '105',
        'width_type' => 'px',
        'parent' => 'main-row',
        'class' => '',
      ),
    ),
  );
  $export['spotlight_panel'] = $layout;

  return $export;
}
