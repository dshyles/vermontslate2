<?php drupal_add_css(drupal_get_path('module','vss_styles') .'/plugins/styles/application_fieldset/application_fieldset.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE)); ?>
<div class="application_fieldset-region">
  <h2 id="<?php print strtolower($settings['display_title']);?>" class="region-title"><?php print $settings['display_title'];?></h2>
  <fieldset>
    <legend>
      <a href="#page">top</a> |
      <a href="#specifications">specifications</a> |
      <a href="#stones">stones</a>
    </legend>
    <div class="vss-fieldset">
      <?php print render($content); ?>
    </div>
  </fieldset'>
</div>
