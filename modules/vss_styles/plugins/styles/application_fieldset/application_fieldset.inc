<?php

/**
* Implementation of hook_panels_styles().
*/
$plugin =  array(
  'application_fieldset' => array(
    'title' => t('VSS Application Fieldset'),
    'description' => t('Application Fieldset description'),
    'render pane' => 'application_fieldset_render_pane',
    'render region' => 'application_fieldset_render_region',
    'settings form' => 'application_fieldset_settings_form',
    'hook theme' => array(
      'application_fieldset_theme_pane' => array(
        'template' => 'application_fieldset-pane',
        'path' => panels_get_path('plugins/styles/application_fieldset', FALSE, 'vss_styles'),
        'variables' => array(
          'content' => NULL,
        ),
      ),
      'application_fieldset_theme_region' => array(
        'template' => 'application_fieldset-region',
        'path' => panels_get_path('plugins/styles/application_fieldset', FALSE, 'vss_styles'),
        'variables' => array(
          'content' => NULL,
      ),
    ),
  ),
));

/**
 * settings for region
 */
function application_fieldset_settings_form($style_settings) {
  $form['display_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Display Title'),
    '#default_value' => (isset($style_settings['display_title'])) ? $style_settings['display_title'] : '',
  );
  return $form;
}

/**
 * render_pane
 */
function theme_application_fieldset_render_pane($vars) {
  $content = $vars['content'];
  $pane = $vars['pane'];
  $display = $vars['display'];
  return theme('application_fieldset_theme_pane', array('content' => $content, 'pane' => $panel, 'display' => $display));
}

/**
 * render_region
 */
function theme_application_fieldset_render_region($vars) {
  $settings = $vars['settings'];
  $panes = $vars['panes'];
  $content = '';
  foreach ($vars['panes'] as $pane_id => $pane_output) {
    $content .= $pane_output;
  }
  return theme('application_fieldset_theme_region', array('content' => $content, 'settings' => $settings));
}
