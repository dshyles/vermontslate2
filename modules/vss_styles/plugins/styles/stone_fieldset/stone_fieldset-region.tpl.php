<?php drupal_add_css(drupal_get_path('module','vss_styles') .'/plugins/styles/stone_fieldset/stone_fieldset.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE)); ?>
<div class="stone_fieldset-region">
  <h2 id="<?php print strtolower($settings['display_title']);?>" class="region-title"><?php print $settings['display_title'];?></h2>
  <fieldset>
    <legend>
      <a href="#page">stone image</a> |
      <a href="#specifications">specifications</a> |
      <a href="#finishes">finishes</a> |
      <a href="#applications">applications</a> |
      <a href="#projects">projects</a>
    </legend>
    <div class="vss-fieldset">
      <?php print render($content); ?>
    </div>
  </fieldset'>
</div>
