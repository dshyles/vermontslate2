<?php

/**
* Implementation of hook_panels_styles().
*/
$plugin =  array(
  'stone_fieldset' => array(
    'title' => t('VSS Stone Fieldset'),
    'description' => t('Stone Fieldset description'),
    'render pane' => 'stone_fieldset_render_pane',
    'render region' => 'stone_fieldset_render_region',
    'settings form' => 'stone_fieldset_settings_form',
    'hook theme' => array(
      'stone_fieldset_theme_pane' => array(
        'template' => 'stone_fieldset-pane',
        'path' => panels_get_path('plugins/styles/stone_fieldset', FALSE, 'vss_styles'),
        'variables' => array(
          'content' => NULL,
        ),
      ),
      'stone_fieldset_theme_region' => array(
        'template' => 'stone_fieldset-region',
        'path' => panels_get_path('plugins/styles/stone_fieldset', FALSE, 'vss_styles'),
        'variables' => array(
          'content' => NULL,
      ),
    ),
  ),
));

/**
 * settings for region
 */
function stone_fieldset_settings_form($style_settings) {
  $form['display_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Display Title'),
    '#default_value' => (isset($style_settings['display_title'])) ? $style_settings['display_title'] : '',
  );
  return $form;
}

/**
 * render_pane
 */
function theme_stone_fieldset_render_pane($vars) {
  $content = $vars['content'];
  $pane = $vars['pane'];
  $display = $vars['display'];
  return theme('stone_fieldset_theme_pane', array('content' => $content, 'pane' => $pane, 'display' => $display));
}

/**
 * render_region
 */
function theme_stone_fieldset_render_region($vars) {
  $settings = $vars['settings'];
  $panes = $vars['panes'];
  $content = '';
  foreach ($vars['panes'] as $pane_id => $pane_output) {
    $content .= $pane_output;
  }
  return theme('stone_fieldset_theme_region', array('content' => $content, 'settings' => $settings));
}
